#!/usr/bin/env bash

# Show hidden files
defaults write com.apple.finder AppleShowAllFiles YES
killall Finder

# Ask for the administrator password upfront.
sudo -v

# Keep-alive: update existing `sudo` time stamp until the script has finished.
while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &

# The bare necessities
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

# Install GNU core utilities (those that come with OS X are outdated).
# Don’t forget to add `$(brew --prefix coreutils)/libexec/gnubin` to `$PATH`.
brew install coreutils
sudo ln -s /usr/local/bin/gsha256sum /usr/local/bin/sha256sum

# Install some other useful utilities like `sponge`.
brew install moreutils
# Install GNU `find`, `locate`, `updatedb`, and `xargs`, `g`-prefixed.
brew install findutils
# Install GNU `sed`, overwriting the built-in `sed`.
brew install gnu-sed --with-default-names
# Install Bash 4.
# Note: don’t forget to add `/usr/local/bin/bash` to `/etc/shells` before
# running `chsh`.
brew install bash
brew tap homebrew/versions
brew install bash-completion2

# Install `wget` with IRI support.
brew install wget --with-iri

# Install RingoJS and Narwhal.
# Note that the order in which these are installed is important;
# see http://git.io/brew-narwhal-ringo.
brew install ringojs
brew install narwhal

# Install more recent versions of some OS X tools.
brew install vim --override-system-vi
brew install homebrew/dupes/grep
brew install homebrew/dupes/openssh
brew install homebrew/dupes/screen
brew install homebrew/php/php55 --with-gmp

# Install font tools.
brew tap bramstein/webfonttools
brew install sfnt2woff
brew install sfnt2woff-zopfli
brew install woff2

# Install some CTF tools; see https://github.com/ctfs/write-ups.
brew install aircrack-ng
brew install bfg
brew install binutils
brew install binwalk
brew install cifer
brew install dex2jar
brew install dns2tcp
brew install fcrackzip
brew install foremost
brew install hashpump
brew install hydra
brew install john
brew install knock
brew install netpbm
brew install nmap
brew install pngcheck
brew install socat
brew install sqlmap
brew install tcpflow
brew install tcpreplay
brew install tcptrace
brew install ucspi-tcp # `tcpserver` etc.
brew install xpdf
brew install xz

# Install other useful binaries.
brew install ack
brew install dark-mode
#brew install exiv2
brew install git
brew install git-lfs
brew install git-flow
brew install imagemagick --with-webp
brew install lua
brew install lynx
brew install p7zip
brew install pigz
brew install pv
brew install rename
brew install rhino
brew install speedtest_cli
brew install ssh-copy-id
brew install tree
brew install webkit2png
brew install zopfli

# Remove outdated versions from the cellar.
brew cleanup

brew install node
brew install nvm
brew install homebrew/php/composer
brew install caskroom/cask/brew-cask
brew cask install java6
npm -g install gulp
npm -g install grunt
npm -g install grunt-cli

brew install rbenv
sudo gem install compass
sudo gem install compass-core
sudo gem install autoprefixer-rails

# Virtual machines
brew cask install vagrant
brew cask install vagrant-manager
brew cask install virtualbox
vagrant box add 8thom/acquia-php_5.6
vagrant plugin install vagrant-hostsupdater
vagrant plugin install vagrant-vbguest

# Social
brew cask install slack
brew cask install skype

# Passwords
brew cask install dropbox
brew cask install onepassword

# Browsers
brew cask install google-chrome
brew cask install firefox

# Editors
brew cask install textmate
brew cask install phpstorm
brew cask install sublime-text
brew cask install coda

# Development tools
brew cask install cakebrew
brew cask install cyberduck
brew cask install filezilla
brew cask install macgdbp
brew cask install sequel-pro
brew cask install codekit
brew cask install sourcetree
brew cask install soapui

# Quick Look Plugins​
brew cask install qlcolorcode
brew cask install qlstephen
brew cask install qlmarkdown
brew cask install quicklook-json
brew cask install qlprettypatch
brew cask install quicklook-csv
brew cask install betterzipql
brew cask install webpquicklook
brew cask install suspicious-package

# Extra
brew cask install asepsis
brew cask install appcleaner
brew cask install caffeine
brew cask install cheatsheet
brew cask install google-drive
brew cask install flux
brew cask install latexian
brew cask install spectacle
brew cask install superduper
brew cask install vlc
brew cask install textexpander
brew cask install alfred
brew cask install controlplane
brew cask install handbrake
brew cask install iterm2
brew cask install the-unarchiver
brew cask install keka
brew cask install xquartz
brew install wp-cli
brew install awscli

# You'll need this for large spreadsheets that kill Excel - really
brew cask install libreoffice

cat >>~/.bashrc <<EOL
alias git_archive="git archive -o git-\`git log --format="%H" -n 1\`.zip \`git log --format="%H" -n 1\`"
alias count_lines="wc -l"

export NVM_DIR=~/.nvm
source $(brew --prefix nvm)/nvm.sh

if [ -f $(brew --prefix)/etc/bash_completion ]; then
  . $(brew --prefix)/etc/bash_completion
fi

eval "$(rbenv init -)"

PATH="$PATH:~/.composer/vendor/bin"
EOL

# Let's get some projects added to the SSH config file
cat >~/.ssh/config <<EOL
Host renault-stg
    HostName renault-stg-ec2.renaultevents.com.au
    Port 22
    User ec2-user
    IdentityFile ~/.ssh/renault-web-stg.pem

Host renault-ec2
    HostName renault-live-ec2.renaultevents.com.au
    Port 22
    User ec2-user
    IdentityFile ~/.ssh/renault-web-live.pem

Host renault-ec3
    HostName renault-live-ec3.renaultevents.com.au
    Port 22
    User ec2-user
    IdentityFile ~/.ssh/renault-web-live.pem

Host fortywinks
    HostName 54.252.175.165
    Port 22
    User ec2-user
    IdentityFile ~/.ssh/forty.pem

Host fortywinks-proxy
    HostName 54.252.178.160
    Port 22
    User ec2-user
    IdentityFile ~/.ssh/forty.pem

Host parkstreet-cron
    HostName 52.64.112.22
    Port 22
    User ec2-user
    IdentityFile ~/.ssh/parkstreet-cron.pem
EOL
