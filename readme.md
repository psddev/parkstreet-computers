# Park Street computers
Getting computers set up with all the right software usually takes too long. This helps fix that.

packages.sh is based on Sylvain's list of packages and https://github.com/mathiasbynens/dotfiles/blob/master/brew.sh

There's still lots of goodness in https://dotfiles.github.io/ that we can absorb into here.
